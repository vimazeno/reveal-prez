## Requis

[vagrant](https://www.vagrantup.com/)

## lancer

```shell
$ vagrant up
```

[http://127.0.0.1:8000](http://127.0.0.1:8000)

## écrire

* dans `md/prez.md` en markdown
  * deux retours à la ligne pour passer au slide suivant
  * trois retours à la ligne pour créer une nouvelle section

* on peut créer autant de `md/*.md` qu'on veut
  * il suffit d'y faire référence dans index.html

```html

...

<div class="slides">
  <section data-markdown="md/prez.md"
   data-separator="^\n\n\n"
   data-separator-vertical="^\n\n"
   data-separator-notes="^Note:"
   data-charset="utf-8">
 </section>

 <div class="slides">
   <section data-markdown="md/suite-de-prez.md"
    data-separator="^\n\n\n"
    data-separator-vertical="^\n\n"
    data-separator-notes="^Note:"
    data-charset="utf-8">
  </section>
 </div>
</div>

...

```
